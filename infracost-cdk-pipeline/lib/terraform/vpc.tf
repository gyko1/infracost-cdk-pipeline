module "vpc" {
  source = "terraform-aws-modules/vpc/aws"

  name = "vpc-for-server"
  cidr = "10.0.0.0/16"

  azs             = ["ap-northeast-2a"]
  private_subnets = ["10.0.0.0/16"]
}
