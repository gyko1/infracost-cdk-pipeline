terraform {
  backend "s3" {
    key    = "infrastructure.tfstate"
    region = "ap-northeast-2"
  }
}

provider "aws" {
  region = "ap-northeast-2"
}
